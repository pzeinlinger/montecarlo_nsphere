#!/usr/bin/env python3

__author__ = "Paul Zeinlinger"
__version__ = "0.1.0"

import argparse
from logzero import logger
import math
import time
import matplotlib

matplotlib.use("AGG")


class MyRandomGenerator:
    def __init__(self, seed, modulus=2 ** 48, a=25214903917, c=11):
        self.__seed = seed
        self.__modulus = modulus
        self.__a = a
        self.__c = c

    def rand(self):
        self.__seed = (self.__a * self.__seed + self.__c) % self.__modulus
        return self.__seed

    def uni01(self):
        return self.rand() / self.__modulus

    def uni(self, imax):
        return int(self.uni01() * imax)


def main(args):
    import matplotlib.pyplot as plt

    """ Main entry point of the app """
    logger.info("Monte-Carlo n-dimensional Sphere, Version: {}".format(__version__))
    points = 1000000
    seed = int(time.time())
    generator = MyRandomGenerator(seed)

    dimensions = [i for i in range(2, 13)]
    calculation_times = []
    results = []
    for n in dimensions:
        start = time.time()
        counter = 0
        volume = 0

        for i in range(points):
            r2 = 0.0
            for i in range(n):
                r2 += (generator.uni01() - 0.5) ** 2
            r = math.sqrt(r2)
            if r <= 0.5:
                counter += 1

        volume = float(counter) / points
        end = time.time()
        logger.info(
            "Relative volume ({} dimensions): {}, calculation time: {}s".format(
                n, round(volume, 5), round(end - start, 4)
            )
        )
        results.append(volume)
        calculation_times.append(end - start)

    fig, ax1 = plt.subplots()
    ax1.axhline(0, color="black")
    ax1.set_xlabel("dimensions")
    ax1.set_ylabel("volume sphere (r=0.5)/ volume cube (s=1)", color="tab:red")
    ax1.plot(dimensions, results, marker="o", color="tab:red")

    ax2 = ax1.twinx()
    ax2.set_ylabel("calculation time (s)")
    ax2.plot(dimensions, calculation_times, marker="x")

    fig.savefig("out.png")


if __name__ == "__main__":
    """ This is executed when run from the command line """
    parser = argparse.ArgumentParser()

    # Specify output of "--version"
    parser.add_argument(
        "--version",
        action="version",
        version="Monte-Carlo Integral (version {version}) by {author}".format(
            version=__version__, author=__author__
        ),
    )

    args = parser.parse_args()
    main(args)
